package modelo;

public class Calculadora {
    private int capital;
    private int tasa;
    private int agnos;

    public Calculadora(int capital, int tasa, int agnos) {
        this.capital = capital;
        this.tasa = tasa;
        this.agnos = agnos;
    }

    public Calculadora() {
    }

    public int getCapital() {
        return capital;
    }

    public void setCapital(int capital) {
        this.capital = capital;
    }

    public int getTasa() {
        return tasa;
    }

    public void setTasa(int tasa) {
        this.tasa = tasa;
    }

    public int getAgnos() {
        return agnos;
    }

    public void setAgnos(int agnos) {
        this.agnos = agnos;
    }
    
    public int calcularInteres(){
        int resultado=capital*tasa/100*agnos;
        return resultado;
    }
    
}
