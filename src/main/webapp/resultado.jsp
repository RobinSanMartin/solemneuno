<%@page import="modelo.Calculadora"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Calculadora cal=(Calculadora) request.getAttribute("Calculadora");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado</title>
    </head>
    <body>
        <h1>El Interes simple es de: $<%=cal.calcularInteres()%></h1>
    </body>
</html>
