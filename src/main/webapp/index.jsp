<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form name="frm" action="CalculadoraControlador" method="POST">
            <table border="0" >
                <h1>Calculadora de interes simple</h1>
                <tr>
                    <td>Capital</td>
                    <td><input type="text" name="txtCapital"></td>
                </tr>
                <tr>
                    <td>Tasa de interes anual</td>
                    <td><input type="text" name="txtTasa"></td>
                </tr>
                <tr>
                    <td>Años</td>
                    <td><input type="text" name="txtAgnos"></td>
                </tr>
                <tr><td height="50" colspan="2">&nbsp;</td></tr>
                <tr>
                    <td><input type="submit" value="Calcular" name="calcular"></td>
                </tr>  
            </table>
        </form>
    </body>
</html>
